#ifndef PRECOMPILEDHEADERBUILDERCONFIGURATORCONTROL_H
#define PRECOMPILEDHEADERBUILDERCONFIGURATORCONTROL_H

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QLineEdit)
QT_FORWARD_DECLARE_CLASS(QPushButton)

class PrecompiledHeaderBuilder;

class PrecompiledHeaderBuilderConfiguratorControl : public QWidget
{
    Q_OBJECT

public:
    PrecompiledHeaderBuilderConfiguratorControl(QWidget *parent = 0);
    ~PrecompiledHeaderBuilderConfiguratorControl();

    void retranslate();

private:
    void updateBuildTriggerUi();

private slots:
    void selectSourcesHeaderFilesDirPath();
    void selectPrecompiledHeaderFilePath();
    void buildPch();
    void processPchBuilt( const bool sucessfully = true );

private:
    QLabel*      m_sourcesHeadersDirPathLabel;
    QLineEdit*   m_sourcesHeadersDirPathEditor;
    QPushButton* m_sourcesHeadersDirPathSelectorTrigger;

    QLabel*      m_precompiledHeaderFilePathLabel;
    QLineEdit*   m_precompiledHeaderFilePathEditor;
    QPushButton* m_precompiledHeaderFilePathSelectorTrigger;

    QPushButton* m_buildTrigger;

    PrecompiledHeaderBuilder* m_pchBuilder;
};

#endif // PRECOMPILEDHEADERBUILDERCONFIGURATORCONTROL_H
