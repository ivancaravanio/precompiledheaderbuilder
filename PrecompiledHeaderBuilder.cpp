#include "PrecompiledHeaderBuilder.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>

#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    #include <QtConcurrent/QtConcurrentRun>
#else
    #include <QtConcurrentRun>
#endif

const QRegularExpression PrecompiledHeaderBuilder::s_externalHeaderFilePathExtractor( "^[ ]*#[ ]*include[ ]*<.*>" );

PrecompiledHeaderBuilder::PrecompiledHeaderBuilder(
        const QString& aSourcesHeadersDirPath,
        const QString& aPrecompiledHeaderFilePath,
        QObject* const parent )
    : QObject( parent )
    , m_sourcesHeadersDirPath( aSourcesHeadersDirPath )
    , m_precompiledHeaderFilePath( aPrecompiledHeaderFilePath )
    , m_isTraversing( false )
{
}

bool PrecompiledHeaderBuilder::beginBuild()
{
    m_externalHeadersFilePaths.clear();
    m_isTraversing = false;

    const QDir sourcesHeadersDir( m_sourcesHeadersDirPath );
    if ( ! sourcesHeadersDir.exists() )
    {
        return false;
    }

    QFile pchFile( m_precompiledHeaderFilePath );
    if ( ! pchFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        return false;
    }

    this->collectExternalHeaderFilePaths();

    return true;
}

void PrecompiledHeaderBuilder::processExternalHeadersFilePathsCollected()
{
    QFutureWatcher< QSet< QString > >* const watcher = dynamic_cast< QFutureWatcher< QSet< QString > >* >( this->sender() );
    if ( watcher == nullptr )
    {
        return;
    }

    const QSet< QFutureWatcher< QSet< QString > >* >::iterator sourceHeaderFileProcessorWatchersIter = m_sourceHeaderFileProcessorWatchers.find( watcher );
    if ( sourceHeaderFileProcessorWatchersIter == m_sourceHeaderFileProcessorWatchers.end() )
    {
        return;
    }

    const QSet< QString > currentExternalHeadersFilePaths = watcher->result();
    m_externalHeadersFilePaths.unite( currentExternalHeadersFilePaths );
    m_sourceHeaderFileProcessorWatchers.erase( sourceHeaderFileProcessorWatchersIter );
    watcher->deleteLater();

    this->tryCreatePrecompiledHeaderFile();
}

void PrecompiledHeaderBuilder::collectExternalHeaderFilePaths()
{
    m_isTraversing = true;
    this->collectExternalHeaderFilePaths( m_sourcesHeadersDirPath );
    m_isTraversing = false;

    this->tryCreatePrecompiledHeaderFile();
}

void PrecompiledHeaderBuilder::collectExternalHeaderFilePaths( const QString& sourcesHeadersDirPath )
{
    const QDir sourcesHeadersDir( sourcesHeadersDirPath );
    if ( ! sourcesHeadersDir.exists() )
    {
        return;
    }

    // const QFileInfoList items = sourcesHeadersDir.entryInfoList( QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot,
    //                                                              QDir::DirsLast );

    const uint itemsCount = sourcesHeadersDir.count();
    for ( uint itemIndex = 0u; itemIndex < itemsCount; ++ itemIndex )
    {
        const QString childFileDirPathRelative = sourcesHeadersDir[ itemIndex ];

        static const QString currentDirRelative = ".";
        static const QString parentDirRelative = "..";
        if ( childFileDirPathRelative == currentDirRelative
             || childFileDirPathRelative == parentDirRelative )
        {
            continue;
        }

        const QString childFileDirPathAbsolute = sourcesHeadersDir.absoluteFilePath( childFileDirPathRelative );
        const QFileInfo childFileDir( sourcesHeadersDir.absoluteFilePath( childFileDirPathAbsolute ) );
        if ( childFileDir.isDir() )
        {
            this->collectExternalHeaderFilePaths( childFileDirPathAbsolute );
            continue;
        }

        if ( childFileDir.isFile() )
        {
            static const QStringList sourcesHeadersValidSuffixes = QStringList()
                                                                   << "c"
                                                                   << "h"
                                                                   << "cpp"
                                                                   << "hpp"
                                                                   << "cxx"
                                                                   << "hxx";
            if ( ! sourcesHeadersValidSuffixes.contains( childFileDir.suffix(), Qt::CaseInsensitive ) )
            {
                continue;
            }

            QFutureWatcher< QSet< QString > >* const watcher = new QFutureWatcher< QSet< QString > >( this );
            connect( watcher, SIGNAL(finished()), this, SLOT(processExternalHeadersFilePathsCollected()) );
            m_sourceHeaderFileProcessorWatchers.insert( watcher );

            const QFuture< QSet< QString > > future = QtConcurrent::run( & PrecompiledHeaderBuilder::externalHeaderFilePaths,
                                                                         childFileDir.canonicalFilePath() );
            watcher->setFuture( future );
        }
    }
}

QString PrecompiledHeaderBuilder::externalHeaderFilePath( const QString& sourceHeaderFileLine )
{
    const QRegularExpressionMatch match = s_externalHeaderFilePathExtractor.match( sourceHeaderFileLine );
    if ( ! match.hasMatch() )
    {
        return QString();
    }

    const QString lastMatch = match.captured( match.lastCapturedIndex() );
    const int extHeaderFileIndexStart = lastMatch.indexOf( '<' ) + 1;
    const int extHeaderFileIndexEnd = lastMatch.indexOf( '>' ) - 1;
    const QString externalHeaderFilePath = lastMatch.mid( extHeaderFileIndexStart,
                                                          extHeaderFileIndexEnd - extHeaderFileIndexStart + 1 );

    return externalHeaderFilePath.trimmed();
}

void PrecompiledHeaderBuilder::createPrecompiledHeaderFile()
{
    QFile pchFile( m_precompiledHeaderFilePath );
    if ( ! pchFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        return;
    }

    QStringList externalHeaderFilePathsSorted( m_externalHeadersFilePaths.values() );
    externalHeaderFilePathsSorted.sort( Qt::CaseInsensitive );

    {
        QTextStream ts( & pchFile );
        for ( const QString& externalHeaderFilePath : externalHeaderFilePathsSorted )
        {
            ts << "#include <" << externalHeaderFilePath << '>' << endl;
        }
    }

    pchFile.close();

    emit built();
}

void PrecompiledHeaderBuilder::tryCreatePrecompiledHeaderFile()
{
    if ( m_isTraversing
         || ! m_sourceHeaderFileProcessorWatchers.isEmpty() )
    {
        return;
    }

    this->createPrecompiledHeaderFile();
}

QSet< QString > PrecompiledHeaderBuilder::externalHeaderFilePaths( const QString& sourceHeaderFilePath )
{
    QFile sourceHeaderFile( sourceHeaderFilePath );
    if ( ! sourceHeaderFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        return QSet< QString >();
    }

    QSet< QString > externalHeaders;
    while ( sourceHeaderFile.bytesAvailable() > 0 )
    {
        const QString line( sourceHeaderFile.readLine() );
        const QString externalHeaderFilePath = PrecompiledHeaderBuilder::externalHeaderFilePath( line );
        if ( externalHeaderFilePath.isEmpty() )
        {
            continue;
        }

        externalHeaders.insert( externalHeaderFilePath );
    }

    return externalHeaders;
}
