QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PrecompiledHeaderBuilder
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp \
           PrecompiledHeaderBuilder.cpp \
    PrecompiledHeaderBuilderConfiguratorControl.cpp

HEADERS  += \
            PrecompiledHeaderBuilder.h \
    PrecompiledHeaderBuilderConfiguratorControl.h
