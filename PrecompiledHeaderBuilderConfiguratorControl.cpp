#include "PrecompiledHeaderBuilderConfiguratorControl.h"

#include "PrecompiledHeaderBuilder.h"

#include <QDir>
#include <QFileDialog>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>

PrecompiledHeaderBuilderConfiguratorControl::PrecompiledHeaderBuilderConfiguratorControl(QWidget *parent)
    : QWidget(parent)
    , m_sourcesHeadersDirPathLabel( nullptr )
    , m_sourcesHeadersDirPathEditor( nullptr )
    , m_sourcesHeadersDirPathSelectorTrigger( nullptr )
    , m_precompiledHeaderFilePathLabel( nullptr )
    , m_precompiledHeaderFilePathEditor( nullptr )
    , m_precompiledHeaderFilePathSelectorTrigger( nullptr )
    , m_buildTrigger( nullptr )
    , m_pchBuilder( nullptr )
{
    m_sourcesHeadersDirPathLabel = new QLabel;
    m_sourcesHeadersDirPathEditor = new QLineEdit;
    m_sourcesHeadersDirPathSelectorTrigger = new QPushButton;
    connect( m_sourcesHeadersDirPathSelectorTrigger,
             SIGNAL(clicked()),
             this,
             SLOT(selectSourcesHeaderFilesDirPath()) );

    m_precompiledHeaderFilePathLabel = new QLabel;
    m_precompiledHeaderFilePathEditor = new QLineEdit;
    m_precompiledHeaderFilePathSelectorTrigger = new QPushButton;
    connect( m_precompiledHeaderFilePathSelectorTrigger,
             SIGNAL(clicked()),
             this,
             SLOT(selectPrecompiledHeaderFilePath()) );

    m_buildTrigger = new QPushButton;
    connect( m_buildTrigger, SIGNAL(clicked()), this, SLOT(buildPch()) );

    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->addWidget( m_sourcesHeadersDirPathLabel, 0, 0 );
    mainLayout->addWidget( m_sourcesHeadersDirPathEditor, 0, 1 );
    mainLayout->addWidget( m_sourcesHeadersDirPathSelectorTrigger, 0, 2 );

    mainLayout->addWidget( m_precompiledHeaderFilePathLabel, 1, 0 );
    mainLayout->addWidget( m_precompiledHeaderFilePathEditor, 1, 1 );
    mainLayout->addWidget( m_precompiledHeaderFilePathSelectorTrigger, 1, 2 );

    mainLayout->addWidget( m_buildTrigger, 2, 0, 1, 3, Qt::AlignHCenter );

    mainLayout->setColumnStretch( 0, 0 );
    mainLayout->setColumnStretch( 1, 1 );
    mainLayout->setColumnStretch( 2, 0 );

    this->retranslate();
}

PrecompiledHeaderBuilderConfiguratorControl::~PrecompiledHeaderBuilderConfiguratorControl()
{
}

void PrecompiledHeaderBuilderConfiguratorControl::retranslate()
{
    const QString selectText = tr( "Select..." );
    m_sourcesHeadersDirPathLabel->setText( tr( "Sources and headers dir path:" ) );
    m_sourcesHeadersDirPathSelectorTrigger->setText( selectText );

    m_precompiledHeaderFilePathLabel->setText( tr( "Precompiled header file path:" ) );
    m_precompiledHeaderFilePathSelectorTrigger->setText( selectText );
    this->updateBuildTriggerUi();
}

void PrecompiledHeaderBuilderConfiguratorControl::updateBuildTriggerUi()
{
    const bool isBuilding = m_pchBuilder != nullptr;
    m_buildTrigger->setText( isBuilding
                             ? tr( "Building..." )
                             : tr( "Build" ) );
    m_buildTrigger->setEnabled( ! isBuilding );
}

void PrecompiledHeaderBuilderConfiguratorControl::selectSourcesHeaderFilesDirPath()
{
    const QString sourcesHeaderFilesDirPathCandidate = QFileDialog::getExistingDirectory( this );
    if ( sourcesHeaderFilesDirPathCandidate.isEmpty() )
    {
        return;
    }

    m_sourcesHeadersDirPathEditor->setText( QDir::toNativeSeparators( sourcesHeaderFilesDirPathCandidate ) );
}

void PrecompiledHeaderBuilderConfiguratorControl::selectPrecompiledHeaderFilePath()
{
    const QString precompiledHeaderFilePathCandidate = QFileDialog::getSaveFileName( this );
    if ( precompiledHeaderFilePathCandidate.isEmpty() )
    {
        return;
    }

    m_precompiledHeaderFilePathEditor->setText( QDir::toNativeSeparators( precompiledHeaderFilePathCandidate ) );
}

void PrecompiledHeaderBuilderConfiguratorControl::buildPch()
{
    if ( m_pchBuilder != nullptr )
    {
        return;
    }

    m_pchBuilder = new PrecompiledHeaderBuilder( m_sourcesHeadersDirPathEditor->text(),
                                                 m_precompiledHeaderFilePathEditor->text() );
    connect( m_pchBuilder, SIGNAL(built()), this, SLOT(processPchBuilt()) );

    if ( ! m_pchBuilder->beginBuild() )
    {
        this->processPchBuilt( false );
        return;
    }

    this->updateBuildTriggerUi();
}

void PrecompiledHeaderBuilderConfiguratorControl::processPchBuilt( const bool sucessfully )
{
    if ( m_pchBuilder == nullptr )
    {
        return;
    }

    if ( sucessfully )
    {
        QMessageBox::information( this, this->windowTitle(), tr( "PCH built successfully!" ) );
    }
    else
    {
        QMessageBox::critical( this, this->windowTitle(), tr( "Error encountered while building PCH!" ) );
    }

    m_pchBuilder->deleteLater();
    m_pchBuilder = nullptr;

    this->updateBuildTriggerUi();
}
