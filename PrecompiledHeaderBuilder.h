#ifndef PRECOMPILEDHEADERBUILDER_H
#define PRECOMPILEDHEADERBUILDER_H

#include <QFutureWatcher>
#include <QObject>
#include <QRegularExpression>
#include <QSet>
#include <QString>
#include <QStringList>

class PrecompiledHeaderBuilder : public QObject
{
    Q_OBJECT

public:
    explicit PrecompiledHeaderBuilder( const QString& aSourcesHeadersDirPath,
                                       const QString& aPrecompiledHeaderFilePath,
                                       QObject* const parent = nullptr );

    bool beginBuild();

signals:
    void built();

private slots:
    void processExternalHeadersFilePathsCollected();

private:
    void collectExternalHeaderFilePaths();
    void collectExternalHeaderFilePaths( const QString& sourcesHeadersDirPath );

    static QSet< QString > externalHeaderFilePaths( const QString& sourceHeaderFilePath );
    static QString externalHeaderFilePath( const QString& sourceHeaderFileLine );

    void createPrecompiledHeaderFile();
    void tryCreatePrecompiledHeaderFile();

private:
    static const QRegularExpression s_externalHeaderFilePathExtractor;

private:
    QString m_sourcesHeadersDirPath;
    QString m_precompiledHeaderFilePath;
    bool m_isTraversing;
    QSet< QFutureWatcher< QSet< QString > >* > m_sourceHeaderFileProcessorWatchers;

    QSet< QString > m_externalHeadersFilePaths;
};

#endif // PRECOMPILEDHEADERBUILDER_H
